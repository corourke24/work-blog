Rails.application.routes.draw do
 
  mount Ckeditor::Engine => '/ckeditor'
  root 'static_pages#start'
  get '/homedark',  to: 'static_pages#home'
  get '/homelight', to:'static_pages#home-light'
  get '/login',           to: 'sessions#new'
  post '/login',          to: 'sessions#create'
  delete '/logout',       to: 'sessions#destroy'
  get '/contact',         to: 'messages#new', as: 'new_message'
  post '/contact',        to: 'messages#create', as: 'create_message'
  get '/error',           to: 'messages#error_page'
  get '/success',         to: 'messages#success_page'
  get '/portfolio',       to: 'static_pages#portfolio'
  resources :users
  resources :posts
  resources :categories
end
