// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.fullpage
//= require bootstrap-sprockets
//= require ckeditor/init
//= require bootstrap/modal
//= require_tree .

/*global $*/

$(document).ready(function() {
	        $('#fullpage').fullpage({
	         anchors:['firstPage', 'secondPage'],
	          navigation: false,
	          navigationPosition: 'right',
		        controlArrows: false,
		        lazyLoading: true,
		        continuousVertical: true,
		        scrollBar: false,
		        autoScrolling: true,
		        scrollingSpeed: 500,
		        menu: true,
		        
		       afterLoad: function(anchorLink, index) {
		        	 $(".hidden").fadeIn(3600).removeClass('hidden'); 
		        },
	        });
});

$(document).ready(function() {
	        $('#fullpage').fullpage({
	         anchors:['firstPage', 'secondPage'],
	          navigation: false,
	          navigationPosition: 'right',
		        controlArrows: false,
		        lazyLoading: true,
		        continuousVertical: true,
		        scrollBar: false,
		        autoScrolling: true,
		        scrollingSpeed: 500,
		        menu: true,
		        
		       afterLoad: function(anchorLink, index) {
		        	 $(".hidden").fadeIn(3600).removeClass('hidden'); 
		        },
	        });
});

/* Preloader */

$(document).ready(function () {
  $(window).load(function(){
    $('.preloader').delay(400).fadeOut(500);
  })

})

$(document).ready(function () {
	 $('div.hidden').fadeIn(4500).removeClass('hidden');	
});

