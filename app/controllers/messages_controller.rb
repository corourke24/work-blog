class MessagesController < ApplicationController
    def new
        @message = Message.new
    end
    
    def create
    @message = Message.new message_params

        if @message.valid?
            ContactMailer.contact(@message).deliver_now
            flash[:info] = "Thank you for your message!" 
            redirect_to  '/success'
        else
            redirect_to '/error'
        end
    end

  private

  def message_params
    params.require(:message).permit(:name, :email, :content)
  end
end
